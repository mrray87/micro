from bottle import route, run, request, response

import sqlite3 as lite

import argparse as ap

import json


database = 'base.db'

host = 'localhost'
port = 8081


def get_args():
    parser = ap.ArgumentParser()
    parser.add_argument('--host', type=str, default=host, help="Service host url.")
    parser.add_argument('--port', type=int, default=port, help="Service port.")
    args, _ = parser.parse_known_args()
    return args


def db_execute(command):
    res = None
    connection = None
    try:
        connection = lite.connect(database)
        with connection:
            cursor = connection.cursor()
            cursor.execute(command)
            print('Command executed:', command)
            res = cursor.fetchall()
    except lite.Error as e:
        if connection:
            connection.rollback()
            print(e)
            return e
    finally:
        if connection:
            connection.close()
            return res


def initialize():
    tables = db_execute("SELECT * FROM sqlite_master;")
    if "clients" not in tables:
        db_execute("CREATE TABLE clients(first_name TEXT, last_name TEXT)")


@route('/')
def start():
    return '<a href="/clients">Clients</a>'


@route('/ready')
def ready():
    return json.dumps({"result": "ready"})


@route('/clients/flush')
def flush_clients():
    res = db_execute("DELETE FROM clients;")
    return json.dumps({"result": str(res)})


@route('/clients/create')
def create_client():
    first_name = request.query.get("first_name", "")
    last_name = request.query.get("last_name", "")

    if first_name == "" and last_name == "":
        return json.dumps({"result": "Client must have first or last name populated."})

    res = db_execute("INSERT INTO clients VALUES('%s', '%s')" % (first_name, last_name))

    return json.dumps({"result": str(res)})


@route('/clients')
def get_clients():
    clients = db_execute("SELECT * FROM clients;")
    response.content_type = "application/json"
    body = {}
    for i, c in enumerate(clients):
        body[i] = {}
        body[i]["first_name"] = c[0]
        body[i]["last_name"] = c[1]
    return json.dumps(body)


@route('/clients/<client_id>')
def get_clients(client_id):
    clients = db_execute("SELECT * FROM clients WHERE rowid = %s;" % client_id)
    response.content_type = "application/json"

    body = {
        "first_name": clients[0][0],
        "last_name": clients[0][1]
    }

    return json.dumps(body)


def main():
    initialize()

    args = get_args()
    run(host=args.host, port=args.port)


if __name__ == '__main__':
    main()
